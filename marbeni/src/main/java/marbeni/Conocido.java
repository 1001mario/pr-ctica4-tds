package marbeni;

/**
 * Representa un conocido.
 * 
 * Solamente guardamos el dni y el nombre del conocido para identificarlo, sin
 * necesidad de guardar sus amigos o posibles conocidos a su vez, ya que esto es
 * algo que se realiza con las personas y el conocido es algo mas simple.
 * 
 * @author Mario Benito Rodriguez
 *
 */
public class Conocido extends Relacion {

	/**
	 * Crea un conocido con un dni y un nombre determinado.
	 * 
	 * @param dni
	 *            - DNI del conocido (unico) - String
	 * @param nombre
	 *            - Nombre del conocido - String
	 * @throws IllegalArgumentException
	 *             - Si el dni es nulo o vacio, si el dni no tiene 9 cifras, si
	 *             el dni no contiene 8 digitos, si el dni no tiene como ultimo
	 *             valor una letra, si el dni es incorrecto, si el nombre es
	 *             nulo o vacio
	 */
	public Conocido(String dni, String nombre) {
		super(dni, nombre);
	}

	@Override
	public String toString() {
		return "Conocido con dni: " + getDNI() + ", nombre: " + getNombre();
	}
}
