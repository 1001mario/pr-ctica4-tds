package marbeni;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Representa una cola de amigos.
 * 
 * @author Mario Benito Rodriguez
 *
 */
public class ColaAmigos {

	private List<Persona> personas;
	private Map<Persona, Persona[]> reservasEntrada;
	private Map<Persona, Persona[]> reservas;
	private static final int amigosMaximos = 10;

	/**
	 * Crea una cola de amigos vacia.
	 * 
	 */
	public ColaAmigos() {
		personas = new ArrayList<>();
		reservas = new HashMap<>();
		reservasEntrada = new HashMap<>();
	}

	/**
	 * Pide la vez para una persona especifica y un conjunto de amigos a los
	 * cuales va a reservar.
	 * 
	 * Agrega una nueva persona en la cola y tiene en cuenta los amigos a los
	 * cuales ha reservado esa persona.
	 * 
	 * No se puede reservar para mas de 10 personas.
	 * 
	 * Un amigo que ha sido colado por otro no puede a su vez colar a nadie mas.
	 * 
	 * @param persona
	 * @param amigos
	 * @throws IllegalArgumentException
	 *             - Si la persona es un valor nulo, si las personas para las
	 *             que se reservan son nulas, si se han reservado para mas de 10
	 *             amigos, si se intenta reservar para personas ya reservadas,
	 *             si una persona intenta entrar en la cola cuando ya esta, si
	 *             un amigo reservado intenta entrar reservando para otros
	 *             amigos, si se intenta reservar para personas que no son
	 *             realmente amigos de la persona que va a entrar o bien si se
	 *             intenta reservar para personas iguales
	 */
	public void entrarEnCola(Persona persona, Persona[] personas) {
		if (persona == null)
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		if (personas == null)
			throw new IllegalArgumentException(
					"Los amigos que se quieren reservar no pueden ser nulos");
		if (personas.length > 10)
			throw new IllegalArgumentException(
					"No se puede reservar para mas de 10 personas");
		if (this.personas.contains(persona))
			throw new IllegalArgumentException(
					"La persona ya esta en la cola, no puede volver a entrar");
		if (new HashSet<Persona>(Arrays.asList(personas)).size() != personas.length)
			throw new IllegalArgumentException(
					"No se puede reservar para personas iguales");
		ArrayList<Persona> personasReservadas = new ArrayList<>();
		for (Persona[] perReservadas : reservas.values()) {
			for (Persona p : perReservadas) {
				personasReservadas.add(p);
			}
		}
		for (Persona p : personas) {
			if (personasReservadas.contains(p))
				throw new IllegalArgumentException(
						"Ya existe una reserva para esta persona");
		}
		if (personasReservadas.contains(persona) && personas.length > 0)
			throw new IllegalArgumentException(
					"Una persona ya reservada no puede reservar para nadie mas");
		for (Persona p : personas) {
			if (!persona.isAmigo(p)) {
				throw new IllegalArgumentException(
						"Las personas que se quieren reservar no son amigos");
			}
		}
		if (!personasReservadas.contains(persona)) {
			// No esta reservado.
			this.personas.add(persona);
			if (personas.length > 0) {
				reservasEntrada.put(persona, personas);
				reservas.put(persona, personas);
			}
		} else {
			// Esta reservado.
			for (Persona p : reservas.keySet()) {
				if (persona.isAmigo(p)
						&& Arrays.asList(reservas.get(p)).contains(persona)) {
					this.personas.add(this.personas.indexOf(p), persona);
					break;
				}
			}
		}
	}

	/**
	 * Pide la vez para una persona en concreto.
	 * 
	 * Agrega a la persona especifica sin ninguna reserva, la persona entra en
	 * la cola si no es un valor nulo o si no esta en la cola de amigos.
	 * 
	 * @param persona
	 * @throws IllegalArgumentException
	 *             - Si persona es un valor nulo, si se intenta agregar una
	 *             persona ya existente en la cola
	 */
	public void entrarEnCola(Persona persona) {
		if (persona == null)
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		if (this.personas.contains(persona))
			throw new IllegalArgumentException("La persona ya esta en la cola");
		ArrayList<Persona> personasReservadas = new ArrayList<>();
		for (Persona[] perReservadas : reservas.values()) {
			for (Persona p : perReservadas) {
				personasReservadas.add(p);
			}
		}
		if (!personasReservadas.contains(persona)) {
			// No esta reservado.
			this.personas.add(persona);
		} else {
			// Esta reservado.
			for (Persona p : reservas.keySet()) {
				if (persona.isAmigo(p)
						&& Arrays.asList(reservas.get(p)).contains(persona)) {
					this.personas.add(this.personas.indexOf(p), persona);
					break;
				}
			}
		}
	}

	/**
	 * Reserva para una persona que este en la cola una cantidad determinada de
	 * amigos.
	 * 
	 * Si una persona ha reservado al entrar en la cola y se han colado a todos
	 * esos amigos reservados, la persona no puede colar a nadie mas.
	 * 
	 * @param persona
	 *            - Persona de la cola que desea reservar
	 * @throws IllegalArgumentException
	 *             - Si la persona es un valor nulo, si los amigos son nulos o
	 *             vacios, si la persona no esta en la cola, si se intenta
	 *             reservar para amigos cuando se han colado ya a los amigos
	 *             reservados al entrar en la cola, si se intenta colar a gente
	 *             pero no cumple con la condicion de 10 personas coladas en
	 *             total, si existen personas que se quieren reservar que no son
	 *             amigos de la persona que esta en la cola, si los amigos son
	 *             iguales
	 */
	public void reservar(Persona persona, Persona[] personas) {
		if (persona == null)
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		if (personas == null)
			throw new IllegalArgumentException(
					"Los amigos que se quieren reservar no pueden ser nulos");
		if (new HashSet<Persona>(Arrays.asList(personas)).size() != personas.length)
			throw new IllegalArgumentException(
					"No se puede reservar para amigos iguales");
		if (personas.length == 0)
			throw new IllegalArgumentException(
					"Los amigos para los que se reserva no pueden ser vacios");
		if (!this.personas.contains(persona))
			throw new IllegalArgumentException(
					"La persona no esta en la cola y no puede reservar");
		if (reservasEntrada.containsKey(persona)
				&& this.personas.containsAll(Arrays.asList(reservasEntrada
						.get(persona))))
			throw new IllegalArgumentException(
					"No se puede colar a nadie mas porque han entrado los amigos reservados en la entrada");
		if (reservas.containsKey(persona)
				&& ((reservas.get(persona).length + personas.length) > 10))
			throw new IllegalArgumentException(
					"No se pueden reservar para mas de 10 amigos para cada persona");
		for (Persona p : personas) {
			if (!persona.isAmigo(p))
				throw new IllegalArgumentException(
						"Existe una persona que se quiere reservar que no es amigo");
		}
		if (reservas.containsKey(persona)) {
			List<Persona> lista = Arrays.asList(reservas.get(persona));
			lista.addAll(Arrays.asList(personas));
			reservas.put(persona, lista.toArray(new Persona[lista.size()]));
		} else {
			reservas.put(persona, personas);
		}
	}

	/**
	 * Devuelve el numero de amigos que habia reservado la persona al entrar en
	 * la cola.
	 * 
	 * Si la persona esta en la cola pero no ha reservado para ningun amigo, el
	 * metodo no tiene ningun problema e indicara que no tiene amigos reservados
	 * en la entrada.
	 * 
	 * @param persona
	 * @return int
	 * @throws IllegalArgumentException
	 *             - Si la persona es valor nulo, si la persona no esta en la
	 *             cola
	 */
	public int getAmigosReservados(Persona persona) {
		if (persona == null)
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		if (!personas.contains(persona))
			throw new IllegalArgumentException(
					"La persona tiene que estar en la cola para saber los amigos reservados");
		if (reservasEntrada.containsKey(persona))
			return reservasEntrada.get(persona).length;
		else
			return 0;
	}

	/**
	 * Devuelve el numero de amigos que aun puede reservar una persona que este
	 * en la cola.
	 * 
	 * @param persona
	 *            - Persona que haya reservado en la cola
	 * @return int - Numero de amigos reservados
	 * @throws IllegalArgumentException
	 *             - Si la persona es un valor nulo, si la persona no esta en la
	 *             cola
	 */
	public int getAmigosPorReservar(Persona persona) {
		if (persona == null)
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		if (!personas.contains(persona))
			throw new IllegalArgumentException(
					"La persona debe estar en la cola para saber los amigos que aun puede reservar");
		if (reservas.containsKey(persona))
			return amigosMaximos - reservas.get(persona).length;
		else
			return amigosMaximos;
	}

	/**
	 * Devuelve los amigos para los cuales ha reservado una persona que se
	 * encuentran delante de la misma.
	 * 
	 * @param persona
	 *            - Persona de la cola
	 * @return Persona[] - Array de personas
	 * @throws IllegalArgumentException
	 *             - Si la persona es un valor nulo, si la persona no esta en la
	 *             cola
	 */
	public Persona[] getAmigosDelante(Persona persona) {
		if (persona == null)
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		if (!personas.contains(persona))
			throw new IllegalArgumentException(
					"La persona debe estar en la cola para saber los amigos que tiene colados");
		if (!reservas.containsKey(persona))
			throw new IllegalArgumentException(
					"La persona no ha realizado reservas");
		ArrayList<Persona> amigosReservados = new ArrayList<>(
				Arrays.asList(reservas.get(persona)));
		ArrayList<Persona> amigosEnCola = new ArrayList<>();
		for (int i = 0; i < personas.indexOf(persona); i++) {
			if (amigosReservados.contains(personas.get(i))) {
				amigosEnCola.add(personas.get(i));
			}
		}
		return amigosEnCola.toArray(new Persona[amigosEnCola.size()]);
	}

	/**
	 * Devuelve la persona que se encuentra en la primera posicion de la cola de
	 * amigos, es decir, la persona que va a ser atendida.
	 * 
	 * Es necesario que exista al menos una persona en la cola para que esta
	 * operacion funcione correctamente, de lo contrario lanzara una excepcion
	 * de tipo IllegalStateException.
	 * 
	 * @return Persona
	 * @throws IllegalStateException
	 *             - Si no existen personas en la cola de amigos
	 */
	public Persona getPrimeraPersona() {
		if (personas.isEmpty())
			throw new IllegalStateException("La cola de amigos esta vacia");
		return personas.get(0);
	}

	/**
	 * Atiende a la persona que se encuentra en la primera posicion de la cola
	 * de amigos y la elimina de la cola de amigos.
	 * 
	 * Para poder atender a la proxima persona en orden, es necesario que haya
	 * al menos una persona en la cola.
	 * 
	 * @throws IllegalStateException
	 *             - Si no existen personas en la cola
	 */
	public void atender() {
		if (personas.isEmpty())
			throw new IllegalStateException("La cola de amigos esta vacia");
		for (Persona persona : reservas.keySet()) {
			if (Arrays.asList(reservas.get(persona)).contains(personas.get(0))) {
				ArrayList<Persona> reservados = new ArrayList<>(
						Arrays.asList(reservas.get(persona)));
				reservados.remove(personas.get(0));
				reservas.put(persona,
						reservados.toArray(new Persona[reservados.size()]));
				break;
			}
		}
		personas.remove(0);
	}

	/**
	 * Devuelve la cola de amigos con una representacion en forma de array de
	 * personas
	 * 
	 * @return Persona[] - Array de personas
	 */
	public Persona[] getColaAmigos() {
		return personas.toArray(new Persona[personas.size()]);
	}

	public String toString() {
		return personas.toString();
	}
}
