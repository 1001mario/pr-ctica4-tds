package marbeni;

/**
 * Representa una relacion que puede ser de amigo o conocido.
 * 
 * @author Mario Benito Rodriguez
 *
 */
public class Relacion {

	private String dni;
	private String nombre;

	/**
	 * Crea una relacion con un dni (identificador unico) y un nombre
	 * 
	 * @param dni
	 *            - String
	 * @param nombre
	 *            - String
	 */
	public Relacion(String dni, String nombre) {
		if (dni == null)
			throw new IllegalArgumentException("El dni no puede ser nulo");
		if (dni.equals(""))
			throw new IllegalArgumentException("El dni no puede ser vacio");
		if (nombre == null)
			throw new IllegalArgumentException("El nombre no puede ser nulo");
		if (nombre.equals(""))
			throw new IllegalArgumentException("El nombre no puede ser vacio");
		if (dni.length() != 9)
			throw new IllegalArgumentException(
					"El dni tiene que tener 9 cifras");
		try {
			Integer.parseInt(dni.substring(0, 8));
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"El dni tiene que tener 8 digitos en las 8 primeras cifras");
		}
		if (!Character.isLetter(dni.charAt(dni.length() - 1)))
			throw new IllegalArgumentException(
					"El dni tiene que tener en la ultima cifra una letra");
		char[] tablaLetrasDNI = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P',
				'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C',
				'K', 'E' };
		if (tablaLetrasDNI[Integer.parseInt(dni.substring(0, 8)) % 23] != dni
				.charAt(dni.length() - 1))
			throw new IllegalArgumentException(
					"El dni es incorrecto, la letra no corresponde con los digitos");
		this.dni = dni;
		this.nombre = nombre;
	}

	/**
	 * Devuelve el dni del amigo.
	 * 
	 * @return String
	 */
	public String getDNI() {
		return dni;
	}

	/**
	 * Devuelve el nombre del amigo.
	 * 
	 * @return String
	 */
	public String getNombre() {
		return nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object objeto) {
		if (this == objeto)
			return true;
		if (objeto == null)
			return false;
		if (objeto instanceof Persona) {
			Persona persona = (Persona) objeto;
			if (dni.equals(persona.getDNI())
					&& nombre.equals(persona.getNombre()))
				return true;
			else
				return false;
		}
		Relacion relacion = (Relacion) objeto;
		if (dni.equals(relacion.getDNI())
				&& nombre.equals(relacion.getNombre()))
			return true;
		else
			return false;
	}
}