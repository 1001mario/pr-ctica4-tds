package marbeni;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Representa una persona.
 * 
 * Cada una de las personas tienen un identificador unico, pero pueden tener el
 * mismo nombre, no existe un problema en la existencia de personas con el mismo
 * nombre.
 * 
 * Ademas, las personas tienen una serie de amigos y conocidos, que se
 * representan de diferente manera que las personas asociadas a esta clase.
 * 
 * @author Mario Benito Rodriguez
 */
public class Persona {

	private String dni;
	private String nombre;
	private List<Amigo> amigos;
	private List<Conocido> conocidos;

	/**
	 * Crea una persona con sus amigos y conocidos iniciales
	 * 
	 * @param dni
	 *            - dni (identificador unico) de la persona
	 * @param nombre
	 *            - Nombre de la persona
	 * @param amigos
	 *            - Array de amigos
	 * @param conocidos
	 *            - Array de conocidos
	 * @throws IllegalArgumentException
	 *             - Si el dni es nulo o vacio, si el dni no tiene 9 cifras, si
	 *             el dni no tiene 8 digitos, si el dni no tiene como ultimo
	 *             valor una letra, si el dni no es valido, si el nombre es nulo
	 *             o vacio, si los amigos son nulos, si los conocidos son nulos,
	 *             si existen amigos iguales, si existen conocidos iguales o
	 *             bien si hay amigos y conocidos iguales
	 */
	public Persona(String dni, String nombre, Amigo[] amigos,
			Conocido[] conocidos) {
		this(dni, nombre);
		if (amigos == null)
			throw new IllegalArgumentException("Los amigos no pueden ser nulos");
		if (conocidos == null)
			throw new IllegalArgumentException(
					"Los conocidos no pueden ser nulos");
		if (new HashSet<Amigo>(Arrays.asList(amigos)).size() != amigos.length)
			throw new IllegalArgumentException(
					"Una persona no puede tener amigos iguales");
		if (new HashSet<Conocido>(Arrays.asList(conocidos)).size() != conocidos.length)
			throw new IllegalArgumentException(
					"Una persona no puede tener conocidos iguales");
		for (Amigo amigo : amigos) {
			for (Relacion conocido : conocidos) {
				if (amigo.equals(conocido))
					throw new IllegalArgumentException(
							"Una persona no puede tener amigos y conocidos iguales");
			}
		}
		this.amigos.addAll(Arrays.asList(amigos));
		this.conocidos.addAll(Arrays.asList(conocidos));
	}

	/**
	 * Crea una persona con un dni unico y un nombre determinado.
	 * 
	 * No pueden existir dos personas con el mismo dni, aunque si pueden existir
	 * dos personas con el mismo nombre.
	 * 
	 * @param dni
	 *            - Identificador de la persona
	 * @param nombre
	 *            - Nombre de la persona
	 * @throws IllegalArgumentException
	 *             - Si el dni es un valor nulo o vacio, si el dni no tiene 8
	 *             digitos, si el dni no tiene una ultima letra, si el dni es
	 *             incorrecto y no valido (letra no acorde a los digitos), si el
	 *             nombre es nulo o vacio
	 */
	public Persona(String dni, String nombre) {
		if (dni == null)
			throw new IllegalArgumentException("El dni no puede ser nulo");
		if (dni.equals(""))
			throw new IllegalArgumentException("El dni no puede ser vacio");
		if (nombre == null)
			throw new IllegalArgumentException("El nombre no puede ser nulo");
		if (nombre.equals(""))
			throw new IllegalArgumentException("El nombre no puede ser vacio");
		if (dni.length() != 9)
			throw new IllegalArgumentException(
					"El dni tiene que tener 9 cifras");
		try {
			Integer.parseInt(dni.substring(0, 8));
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"El dni tiene que tener 8 digitos en las 8 primeras cifras");
		}
		if (!Character.isLetter(dni.charAt(dni.length() - 1)))
			throw new IllegalArgumentException(
					"El dni tiene que tener en la ultima cifra una letra");
		char[] tablaLetrasDNI = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P',
				'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C',
				'K', 'E' };
		if (tablaLetrasDNI[Integer.parseInt(dni.substring(0, 8)) % 23] != dni
				.charAt(dni.length() - 1))
			throw new IllegalArgumentException(
					"El dni es incorrecto, la letra no corresponde con los digitos");
		this.dni = dni;
		this.nombre = nombre;
		this.amigos = new ArrayList<Amigo>();
		this.conocidos = new ArrayList<Conocido>();
	}

	/**
	 * Devuelve el dni de la persona.
	 * 
	 * @return String
	 */
	public String getDNI() {
		return dni;
	}

	/**
	 * Devuelve el nombre de la persona.
	 * 
	 * @return String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Conoce a un nuevo conocido.
	 * 
	 * @param conocido
	 *            - conocido nuevo
	 * @throws IllegalArgumentException
	 *             - Si el conocido ya existe, si el conocido coincide con
	 *             alguno de los amigos, si el conocido es un valor nulo
	 */
	public void agregarConocido(Conocido conocido) {
		if (conocido == null)
			throw new IllegalArgumentException(
					"El conocido no puede ser un valor nulo");
		if (amigos.contains(conocido))
			throw new IllegalArgumentException(
					"El conocido es un amigo y no se puede incluir como conocido");
		if (conocidos.contains(conocido))
			throw new IllegalArgumentException("El conocido ya existe");
		conocidos.add(conocido);
	}

	/**
	 * Hace un nuevo amigo.
	 * 
	 * Necesita que el amigo haya sido un conocido hasta ahora (al llamar a este
	 * metodo), esta es una condicion indispensable para realizar amigos.
	 * 
	 * @param amigo
	 *            - amigo nuevo
	 * @throws IllegalArgumentException
	 *             - Si el amigo es un valor nulo, si el amigo estaba ya en
	 *             amigos, si el amigo no era un conocido y por tanto no estaba
	 *             en conocidos de la persona
	 */
	public void agregarAmigo(Amigo amigo) {
		if (amigo == null)
			throw new IllegalArgumentException(
					"El amigo no puede ser un valor nulo");
		if (amigos.contains(amigo))
			throw new IllegalArgumentException("El amigo ya esta en amigos");
		if (!conocidos.contains(amigo))
			throw new IllegalArgumentException(
					"El amigo no era un conocido y por tanto no puede ser un nuevo amigo");
		conocidos.remove(amigo);
		amigos.add(amigo);
	}

	/**
	 * Deja de ser amigo de una persona determinada, convirtiendose de nuevo esa
	 * persona en un conocido.
	 * 
	 * @param amigo
	 *            - amigo de la persona
	 * @throws IllegalArgumentException
	 *             - Si el amigo es un valor nulo
	 */
	public void eliminarAmigo(Amigo amigo) {
		if (amigo == null)
			throw new IllegalArgumentException(
					"El amigo no puede ser un valor nulo");
		amigos.remove(amigo);
		conocidos.add(new Conocido(amigo.getDNI(), amigo.getNombre()));
	}

	/**
	 * Indica si otra persona es amigo.
	 * 
	 * Mira a ver si ambas personas se tienen como amigos y en el que caso de
	 * que sea asi, indica que ambas personas son amigos.
	 * 
	 * ¡Ojo! De lo contrario, si una de las personas no tiene a la otra como
	 * amigo, no se va a indicar que ambos sean amigos.
	 * 
	 * @param persona
	 *            - persona con amigos
	 * @return boolean
	 * @throws IllegalArgumentException
	 *             - Si el argumento persona es un valor nulo
	 */
	public boolean isAmigo(Persona persona) {
		if (persona == null)
			throw new IllegalArgumentException(
					"La persona no puede ser un valor nulo");
		if (amigos.contains(persona)
				&& Arrays.asList(persona.getAmigos()).contains(this))
			return true;
		else
			return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amigos == null) ? 0 : amigos.hashCode());
		result = prime * result
				+ ((conocidos == null) ? 0 : conocidos.hashCode());
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object objeto) {
		if (this == objeto)
			return true;
		if (objeto == null)
			return false;
		if (objeto instanceof Amigo) {
			Amigo amigo = (Amigo) objeto;
			if (dni.equals(amigo.getDNI()) && nombre.equals(amigo.getNombre()))
				return true;
			else
				return false;
		}
		if (objeto instanceof Conocido) {
			Conocido conocido = (Conocido) objeto;
			if (dni.equals(conocido.getDNI())
					&& nombre.equals(conocido.getNombre()))
				return true;
			else
				return false;
		}
		Persona persona = (Persona) objeto;
		if (dni.equals(persona.dni) && nombre.equals(persona.nombre)
				&& amigos.equals(persona.amigos)
				&& conocidos.equals(persona.conocidos))
			return true;
		else
			return false;
	}

	@Override
	public String toString() {
		return "Persona con dni: " + dni + ", nombre:" + nombre + ", amigos:"
				+ amigos + ", conocidos: " + conocidos;
	}

	/**
	 * Devuelve los amigos de la persona.
	 * 
	 * @return Persona[] - Array con amigos
	 */
	public Amigo[] getAmigos() {
		return amigos.toArray(new Amigo[amigos.size()]);
	}

	/**
	 * Devuelve los conocidos de la persona.
	 * 
	 * @return Conocido[] - Array de conocidos
	 */
	public Conocido[] getConocidos() {
		return conocidos.toArray(new Conocido[conocidos.size()]);
	}
}