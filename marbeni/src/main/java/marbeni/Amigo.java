package marbeni;

/**
 * Representa un amigo especifico.
 * 
 * Los amigos solo guardan el dni y el nombre propio.
 * 
 * Con esta clase, no se representan los amigos y conocidos de los amigos, si no
 * solamente almacena el identificador del amigo en sí correspondiente a la
 * persona especifica.
 * 
 * @author Mario Benito Rodriguez
 *
 */
public class Amigo extends Relacion {

	/**
	 * Crea un amigo con un dni y nombre determinado.
	 * 
	 * @param dni
	 *            - DNI del amigo (unico) - String
	 * @param nombre
	 *            - Nombre del amigo - String
	 * @throws IllegalArgumentException
	 *             - Si el dni es nulo o vacio, si el dni no tiene 9 cifras, si
	 *             el dni no contiene 8 digitos, si el dni no tiene como ultimo
	 *             valor una letra, si el dni es incorrecto, si el nombre es
	 *             nulo o vacio
	 */
	public Amigo(String dni, String nombre) {
		super(dni, nombre);
	}

	@Override
	public String toString() {
		return "Amigo con dni: " + getDNI() + ", nombre: " + getNombre();
	}
}
