package marbeni;

import static org.junit.Assert.*;

import org.junit.Test;

public class AmigoTest {
	
	@Test
	public void crearAmigoValido () {
		Amigo amigo = new Amigo("71154879D", "Mario");
		assertEquals(amigo.getDNI(), "71154879D");
		assertEquals(amigo.getNombre(), "Mario");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearAmigoDNINull() {
		Amigo amigo = new Amigo(null, "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearAmigoDNIVacio() {
		Amigo amigo = new Amigo("", "Mario");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearAmigoDNINoNueveCifras() {
		Amigo amigo = new Amigo("1234567895463L", "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearAmigoDNINoOchoDigitos() {
		Amigo amigo = new Amigo("1234567", "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearAmigoDNINoUltimaLetra() {
		Amigo amigo = new Amigo("123456789", "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearAmigoDNILetraNoValida() {
		Amigo amigo = new Amigo("12345678K", "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearAmigoNombreNull() {
		Amigo amigo = new Amigo("71154879D", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearAmigoNombreVacio() {
		Amigo amigo = new Amigo("71154879D", "");
	}
}
