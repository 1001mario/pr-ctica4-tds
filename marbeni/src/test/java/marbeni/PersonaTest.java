package marbeni;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;

public class PersonaTest {

	private Persona persona;

	@Before
	public void setUp() throws Exception {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		persona = new Persona("71154879D", "Mario", amigos, conocidos);
		// Creamos la persona para probar los metodos.
	}

	@After
	public void tearDown() throws Exception {
		persona = null;
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNINull() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona(null, "Mario", amigos, conocidos);
		// identificador y dni de la persona no puede ser nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNINoNueveCifras() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("1234567457457547457", "Mario", amigos,
				conocidos);
		// dni tiene que tener 9 cifras
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNINoOchoDigitos() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("1234567", "Mario", amigos, conocidos);
		// dni tiene que tener 8 digitos y una ultima letra.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNINoUltimaLetra() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("123456789", "Mario", amigos, conocidos);
		// dni tiene que tener 8 digitos y una ultima letra.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNILetraNoValida() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("12345678K", "Mario", amigos, conocidos);
		// dni tiene que ser correcto y valido (letra acorde a los digitos).
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaNombreNull() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154869D", null, amigos, conocidos);
		// nombre no puede ser nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaNombreVacio() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "", amigos, conocidos);
		// nombre no puede ser vacio.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaAmigosNull() {
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", null, conocidos);
		// amigos no puede ser un valor nulo
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConocidosNull() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		persona = new Persona("71154879D", "Mario", amigos, null);
		// conocidos no puede ser un valor nulo
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaAmigosConocidosIguales() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("12345678Z", "Pepe"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		persona = new Persona("71154879D", "Mario", amigos, conocidos);
		// Pepe esta tanto en amigos como conocidos.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaAmigosIguales() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("12345678Z", "Pepe") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		persona = new Persona("71154879D", "Mario", amigos, conocidos);
		// Pepe esta repetido dos veces en amigos.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConocidosIguales() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("16422435K", "Pedro"),
				new Conocido("64218524V", "Ruben") };
		persona = new Persona("71154879D", "Mario", amigos, conocidos);
		// Pedro esta repetido dos veces en conocidos.
	}

	@Test
	public void crearPersonaValida() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("64218524V", "Ruben") };
		persona = new Persona("71154879D", "Mario", amigos, conocidos);
		assertEquals(persona.getDNI(), "71154879D");
		assertEquals(persona.getNombre(), "Mario");
	}

	@Test
	public void agregarConocidoNoExistente() {
		persona.agregarConocido(new Conocido("84723593B", "Senmao"));
		assertNotNull(persona.getConocidos());
		Relacion[] conocidosObtenidos = persona.getConocidos();
		Relacion[] conocidosEsperados = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben"),
				new Conocido("84723593B", "Senmao") };
		assertArrayEquals(conocidosEsperados, conocidosObtenidos);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarConocidoExistenteEnConocidos() {
		persona.agregarConocido(new Conocido("16422435K", "Pedro"));
		// Pedro ya es un conocido.
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarConocidoExistenteEnAmigos() {
		persona.agregarConocido(new Conocido("12345678Z", "Pepe"));
		// Pepe es un amigo y no puede ser agregado como conocido.
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarConocidoNull() {
		persona.agregarConocido(null);
		// no se pueden agregar conocidos nulos.
	}

	@Test
	public void agregarAmigoExistenteEnConocidos() {
		persona.agregarAmigo(new Amigo("16422435K", "Pedro"));
		// Pedro pasa de ser conocido a amigo.
		Amigo[] amigosObtenidos = persona.getAmigos();
		assertNotNull(amigosObtenidos);
		Amigo[] amigosEsperados = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("16422435K", "Pedro") };
		Relacion[] conocidosEsperados = { new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Relacion[] conocidosObtenidos = persona.getConocidos();
		assertNotNull(conocidosObtenidos);
		assertArrayEquals(amigosEsperados, amigosObtenidos);
		assertArrayEquals(conocidosEsperados, conocidosObtenidos);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAmigoNoExistenteEnConocidos() {
		persona.agregarAmigo(new Amigo("84723593B", "Senmao"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAmigoExistenteEnAmigos() {
		persona.agregarAmigo(new Amigo("12345678Z", "Pepe"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAmigoNull() {
		persona.agregarAmigo(null);
	}

	@Test
	public void eliminarAmigoExistente() {
		assertNotNull(persona.getAmigos());
		persona.eliminarAmigo(new Amigo("12345678Z", "Pepe"));
		// Pepe pasa de ser amigo a conocido.
		Amigo[] amigos = persona.getAmigos();
		assertNotNull(amigos);
		Amigo[] amigosEsperados = { new Amigo("54673829T", "Juan") };
		Relacion[] conocidosObtenidos = persona.getConocidos();
		Relacion[] conocidosEsperados = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben"),
				new Conocido("12345678Z", "Pepe") };
		assertArrayEquals(amigosEsperados, amigos);
		assertArrayEquals(conocidosEsperados, conocidosObtenidos);
		// Al eliminar a Pepe como amigo, Pepe se convierte en un conocido para
		// Mario.
	}

	@Test
	public void eliminarAmigoNoExistente() {
		persona.eliminarAmigo(new Amigo("84723593B", "Senmao"));
		// No hay problema, porque si no existe no se borra ni ocurre ninguna
		// excepcion
		assertNotNull(persona.getAmigos());
		Amigo[] amigosObtenidos = persona.getAmigos();
		Amigo[] amigosEsperados = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		assertArrayEquals(amigosEsperados, amigosObtenidos);
	}

	@Test(expected = IllegalArgumentException.class)
	public void eliminarAmigoNull() {
		persona.eliminarAmigo(null);
	}

	@Test
	public void isAmigoExistenteAmbosAmigos() {
		Persona persona2 = new Persona("54673829T", "Juan",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] {});
		boolean isAmigo = persona.isAmigo(persona2);
		// Juan tiene a Mario como amigo y Mario tiene a Juan como amigo
		// Por tanto, son ambos amigos y Mario es amigo de Juan.
		assertNotNull(persona.getAmigos());
		assertNotNull(persona2.getAmigos());
		assertTrue(isAmigo);
	}

	@Test
	public void isAmigoExistenteNoAmigos() {
		boolean isAmigo = persona.isAmigo(new Persona("54673829T", "Juan"));
		// Mario tiene a Juan como amigo pero Juan no tiene a Mario como amigo.
		// Por definicion, Mario no es amigo de Juan porque ambos no son amigos
		// mutuamente.
		assertNotNull(persona.getAmigos());
		assertFalse(isAmigo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void isAmigoNull() {
		boolean isAmigo = persona.isAmigo(null);
	}

	@Test
	public void getAmigosPersona() {
		Amigo[] amigos = persona.getAmigos();
		assertNotNull(amigos);
		Amigo[] amigosEsperados = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		assertArrayEquals(amigosEsperados, amigos);
	}
}
