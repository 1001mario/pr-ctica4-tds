package marbeni;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ColaAmigosMockTest.class, PersonaMockTest.class })
public class EjecutarTestEnAislamiento {

}
