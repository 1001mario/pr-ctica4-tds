package marbeni;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;

import org.easymock.*;

public class PersonaMockTest {

	private Persona persona;

	@Mock
	private Amigo amigo1;
	private Amigo amigo2;
	private Conocido conocido1;
	private Conocido conocido2;
	private Conocido conocido3;

	@Before
	public void setUp() throws Exception {
		amigo1 = createMock(Amigo.class);
		amigo2 = createMock(Amigo.class);
		conocido1 = createMock(Conocido.class);
		conocido2 = createMock(Conocido.class);
		conocido3 = createMock(Conocido.class);
		expect(amigo1.getDNI()).andReturn("12345678Z").anyTimes();
		expect(amigo1.getNombre()).andReturn("Pepe").anyTimes();
		expect(amigo2.getDNI()).andReturn("54673829T").anyTimes();
		expect(amigo2.getNombre()).andReturn("Juan").anyTimes();
		expect(conocido1.getDNI()).andReturn("16422435K").anyTimes();
		expect(conocido1.getNombre()).andReturn("Pedro").anyTimes();
		expect(conocido2.getDNI()).andReturn("58932789G").anyTimes();
		expect(conocido2.getNombre()).andReturn("Jorge").anyTimes();
		expect(conocido3.getDNI()).andReturn("64218524V").anyTimes();
		expect(conocido3.getNombre()).andReturn("Ruben").anyTimes();
		replay(amigo1);
		replay(amigo2);
		replay(conocido1);
		replay(conocido2);
		replay(conocido3);
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		persona = new Persona("71154879D", "Mario", amigos, conocidos);
	}

	@After
	public void tearDown() throws Exception {
		persona = null;
		amigo1 = null;
		amigo2 = null;
		conocido1 = null;
		conocido2 = null;
		conocido3 = null;
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNINull() {
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		Persona persona = new Persona(null, "Mario", amigos, conocidos);
		// identificador y dni de la persona no puede ser nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNINoNueveCifras() {
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		Persona persona = new Persona("1234567457457547457", "Mario", amigos,
				conocidos);
		// dni tiene que tener 9 cifras
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNINoOchoDigitos() {
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		Persona persona = new Persona("1234567", "Mario", amigos, conocidos);
		// dni tiene que tener 8 digitos y una ultima letra.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNINoUltimaLetra() {
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		Persona persona = new Persona("123456789", "Mario", amigos, conocidos);
		// dni tiene que tener 8 digitos y una ultima letra.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaDNILetraNoValida() {
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		Persona persona = new Persona("12345678K", "Mario", amigos, conocidos);
		// dni tiene que ser correcto y valido (letra acorde a los digitos).
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaNombreNull() {
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		Persona persona = new Persona("71154869D", null, amigos, conocidos);
		// nombre no puede ser nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaNombreVacio() {
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		Persona persona = new Persona("71154879D", "", amigos, conocidos);
		// nombre no puede ser vacio.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaAmigosNull() {
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		Persona persona = new Persona("71154879D", "Mario", null, conocidos);
		// amigos no puede ser un valor nulo
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConocidosNull() {
		Amigo[] amigos = { amigo1, amigo2 };
		persona = new Persona("71154879D", "Mario", amigos, null);
		// conocidos no puede ser un valor nulo
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaAmigosIguales() {
		Amigo[] amigos = { amigo1, amigo1 };
		Conocido[] conocidos = { conocido1, conocido2, conocido3 };
		persona = new Persona("71154879D", "Mario", amigos, conocidos);
		// Pepe esta repetido dos veces en amigos.
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConocidosIguales() {
		Amigo[] amigos = { amigo1, amigo2 };
		Conocido[] conocidos = { conocido1, conocido1, conocido3 };
		persona = new Persona("71154879D", "Mario", amigos, conocidos);
		// Pedro esta repetido dos veces en conocidos.
	}

	@Test
	public void crearPersonaValida() {
		persona = new Persona("71154879D", "Mario", new Amigo[] { amigo1,
				amigo2 }, new Conocido[] { conocido1, conocido2 });
		assertEquals(persona.getDNI(), "71154879D");
		assertEquals(persona.getNombre(), "Mario");
	}

	@Test
	public void agregarConocidoNoExistente() {
		Conocido conocido = createMock(Conocido.class);
		expect(conocido.getDNI()).andReturn("84723593B").anyTimes();
		expect(conocido.getNombre()).andReturn("Senmao").anyTimes();
		replay(conocido);
		persona.agregarConocido(conocido);
		assertNotNull(persona.getAmigos());
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarConocidoExistenteEnConocidos() {
		persona.agregarConocido(conocido1);
		// Pedro ya es un conocido.
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarConocidoNull() {
		persona.agregarConocido(null);
		// no se pueden agregar conocidos nulos.
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAmigoNoExistenteEnConocidos() {
		Amigo amigo = createMock(Amigo.class);
		expect(amigo.getDNI()).andReturn("84723593B").anyTimes();
		expect(amigo.getNombre()).andReturn("Senmao").anyTimes();
		replay(amigo);
		persona.agregarAmigo(amigo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAmigoExistenteEnAmigos() {
		persona.agregarAmigo(amigo1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarAmigoNull() {
		persona.agregarAmigo(null);
	}

	@Test
	public void eliminarAmigoExistente() {
		assertNotNull(persona.getAmigos());
		persona.eliminarAmigo(amigo1);
		// Pepe pasa de ser amigo a conocido.
	}

	@Test
	public void eliminarAmigoNoExistente() {
		Amigo amigo = createMock(Amigo.class);
		expect(amigo.getDNI()).andReturn("84723593B").anyTimes();
		expect(amigo.getNombre()).andReturn("Senmao").anyTimes();
		replay(amigo);
		persona.eliminarAmigo(amigo);
		// No hay problema, porque si no existe no se borra ni ocurre ninguna
		// excepcion
		assertNotNull(persona.getAmigos());
	}

	@Test(expected = IllegalArgumentException.class)
	public void eliminarAmigoNull() {
		persona.eliminarAmigo(null);
	}

	@Test
	public void isAmigoExistenteAmbosAmigos() {
		Amigo amigo = createMock(Amigo.class);
		expect(amigo.getDNI()).andReturn("71154879D").anyTimes();
		expect(amigo.getNombre()).andReturn("Mario").anyTimes();
		replay(amigo);
		Persona persona2 = new Persona("54673829T", "Juan",
				new Amigo[] { amigo },
				new Conocido[] {});
		boolean isAmigo = persona.isAmigo(persona2);
		// Juan tiene a Mario como amigo y Mario tiene a Juan como amigo
		// Por tanto, son ambos amigos y Mario es amigo de Juan.
		assertNotNull(persona.getAmigos());
		assertNotNull(persona2.getAmigos());
		assertTrue(isAmigo);
	}

	@Test
	public void isAmigoExistenteNoAmigos() {
		boolean isAmigo = persona.isAmigo(new Persona("54673829T", "Juan"));
		// Mario tiene a Juan como amigo pero Juan no tiene a Mario como amigo.
		// Por definicion, Mario no es amigo de Juan porque ambos no son amigos
		// mutuamente.
		assertNotNull(persona.getAmigos());
		assertFalse(isAmigo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void isAmigoNull() {
		boolean isAmigo = persona.isAmigo(null);
	}

	@Test
	public void getAmigosPersona() {
		Amigo[] amigos = persona.getAmigos();
		assertNotNull(amigos);
	}

}
