package marbeni;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;

public class ColaAmigosTest {

	private ColaAmigos colaAmigos;

	@Before
	public void setUp() throws Exception {
		colaAmigos = new ColaAmigos();
	}

	@After
	public void tearDown() throws Exception {
		colaAmigos = null;
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaYaDentroSinReserva() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.entrarEnCola(persona);
		// Entra por primera vez.
		colaAmigos.entrarEnCola(persona);
		// persona no puede volver a entrar a la cola si ya esta dentro.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaNullSinReserva() {
		colaAmigos.entrarEnCola(null);
		// persona no puede ser null.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaNullConReserva() {
		Persona[] amigos = { new Persona("12345678Z", "Pepe"),
				new Persona("54673829T", "Juan") };
		colaAmigos.entrarEnCola(null, amigos);
		// persona no puede ser null.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaAmigosNullConReserva() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.entrarEnCola(persona, null);
		// amigos no pueden ser valores nulos.
	}

	@Test
	public void entrarEnColaConMenosDeDiezAmigos() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("58932789G", "Jorge") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("64218524V", "Ruben"),
						new Conocido("16422435K", "Pedro") });
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Cumple con las condiciones.
		// Los amigos para los que se reservan no son mas de 10.
		for (Persona p : amigosReservados) {
			colaAmigos.entrarEnCola(p);
			// Los amigos entran y como estan reservados, se adelantan en la
			// cola.
		}
		Persona[] colaAmigosEsperada = { amigo1, amigo2, persona };
		// Este es el orden de la cola tras la entrada de la persona y amigos.
		Persona[] colaAmigosObtenida = colaAmigos.getColaAmigos();
		assertNotNull(colaAmigos.getColaAmigos());
		assertArrayEquals(colaAmigosEsperada, colaAmigosObtenida);
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonasIguales() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("58932789G", "Jorge") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona[] amigosReservados = new Persona[] { amigo1, amigo1 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Se intenta reservar para dos personas iguales!
	}

	@Test
	public void entrarEnColaConDiezAmigos() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"),
				new Amigo("16422435K", "Pedro"),
				new Amigo("58932789G", "Jorge"),
				new Amigo("64218524V", "Ruben"), new Amigo("15377228A", "Ana"),
				new Amigo("84723593B", "Senmao"),
				new Amigo("56832445C", "Regino"),
				new Amigo("12367843F", "Daniel"),
				new Amigo("67832459P", "Sara") };
		Persona persona = new Persona("71154879D", "Mario", amigos,
				new Conocido[] {});
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("58932789G", "Jorge") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("64218524V", "Ruben"),
						new Conocido("16422435K", "Pedro") });
		Persona amigo3 = new Persona("16422435K", "Pedro",
				new Amigo[] { new Amigo("71154879D", "Mario"),
						new Amigo("67832459P", "Sara") },
				new Conocido[] { new Conocido("12367843F", "Daniel") });
		Persona amigo4 = new Persona("58932789G", "Jorge",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("56832445C", "Regino") });
		Persona amigo5 = new Persona("64218524V", "Ruben", new Amigo[] {
				new Amigo("84723593B", "Senmao"),
				new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("54673829T", "Juan") });
		Persona amigo6 = new Persona("15377228A", "Ana",
				new Amigo[] { new Amigo("67832459P", "Sara"),
						new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("84723593B", "Senmao") });
		Persona amigo7 = new Persona("84723593B", "Senmao",
				new Amigo[] { new Amigo("12345678Z", "Pepe"),
						new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("16422435K", "Pedro") });
		Persona amigo8 = new Persona("56832445C", "Regino",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("15377228A", "Ana") });
		Persona amigo9 = new Persona("12367843F", "Daniel",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] {});
		Persona amigo10 = new Persona("67832459P", "Sara", new Amigo[] {
				new Amigo("58932789G", "Jorge"),
				new Amigo("71154879D", "Mario") }, new Conocido[] {});
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2, amigo3,
				amigo4, amigo5, amigo6, amigo7, amigo8, amigo9, amigo10 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Todos son amigos de Mario.
		for (Persona p : amigosReservados) {
			colaAmigos.entrarEnCola(p);
		}
		Persona[] colaAmigosEsperada = { amigo1, amigo2, amigo3, amigo4,
				amigo5, amigo6, amigo7, amigo8, amigo9, amigo10, persona };
		Persona[] colaAmigosObtenida = colaAmigos.getColaAmigos();
		assertNotNull(colaAmigos.getColaAmigos());
		assertArrayEquals(colaAmigosEsperada, colaAmigosObtenida);
		// Cumple con las condiciones.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaConMasDe10Amigos() {
		Amigo[] amigos = new Amigo[] { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"),
				new Amigo("16422435K", "Pedro"),
				new Amigo("58932789G", "Jorge"),
				new Amigo("64218524V", "Ruben"), new Amigo("15377228A", "Ana"),
				new Amigo("84723593B", "Senmao"),
				new Amigo("56832445C", "Regino"),
				new Amigo("12367843F", "Daniel"),
				new Amigo("67832459P", "Sara"),
				new Amigo("87935167A", "Manuel") };
		// 11 amigos.
		Persona persona = new Persona("71154879D", "Mario", amigos,
				new Conocido[] {});
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("58932789G", "Jorge") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("64218524V", "Ruben"),
						new Conocido("16422435K", "Pedro") });
		Persona amigo3 = new Persona("16422435K", "Pedro", new Amigo[] {
				new Amigo("56832445C", "Regino"),
				new Amigo("67832459P", "Sara") },
				new Conocido[] { new Conocido("12367843F", "Daniel") });
		Persona amigo4 = new Persona("58932789G", "Jorge",
				new Amigo[] { new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("71154879D", "Mario") });
		Persona amigo5 = new Persona("64218524V", "Ruben",
				new Amigo[] { new Amigo("84723593B", "Senmao") },
				new Conocido[] { new Conocido("54673829T", "Juan") });
		Persona amigo6 = new Persona("15377228A", "Ana",
				new Amigo[] { new Amigo("67832459P", "Sara") },
				new Conocido[] { new Conocido("84723593B", "Senmao") });
		Persona amigo7 = new Persona("84723593B", "Senmao",
				new Amigo[] { new Amigo("12345678Z", "Pepe"),
						new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("16422435K", "Pedro") });
		Persona amigo8 = new Persona("56832445C", "Regino",
				new Amigo[] { new Amigo("12345678Z", "Pepe") },
				new Conocido[] { new Conocido("15377228A", "Ana") });
		Persona amigo9 = new Persona("12367843F", "Daniel");
		Persona amigo10 = new Persona("67832459P", "Sara",
				new Amigo[] { new Amigo("58932789G", "Jorge") },
				new Conocido[] {});
		Persona amigo11 = new Persona("87935167A", "Manuel");
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2, amigo3,
				amigo4, amigo5, amigo6, amigo7, amigo8, amigo9, amigo10,
				amigo11 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// No se permite reservar para mas de 10 amigos.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaYaDentro() {
		Amigo[] amigos = new Amigo[] { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"),
				new Amigo("16422435K", "Pedro"),
				new Amigo("58932789G", "Jorge"),
				new Amigo("64218524V", "Ruben") };
		// 5 amigos.
		Persona persona = new Persona("71154879D", "Mario", amigos,
				new Conocido[] { new Conocido("87935167A", "Manuel") });
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("58932789G", "Jorge") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("64218524V", "Ruben"),
						new Conocido("16422435K", "Pedro") });
		Persona amigo3 = new Persona("16422435K", "Pedro",
				new Amigo[] { new Amigo("71154879D", "Mario"),
						new Amigo("67832459P", "Sara") },
				new Conocido[] { new Conocido("12367843F", "Daniel") });
		Persona amigo4 = new Persona("58932789G", "Jorge",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("56832445C", "Regino") });
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra a la cola de amigos y reserva para dos amigos.
		// Aqui no hay ninguna excepcion ni ningun problema.
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo3, amigo4 });
		// Se vuelve a intentar entrar en la cola por parte de la persona Mario.
		// Pero no puede volver a entrar y realizar reserva en la cola porque ya
		// esta dentro.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaNoPuedeReservar() {
		Amigo[] amigos = new Amigo[] { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"),
				new Amigo("16422435K", "Pedro"),
				new Amigo("58932789G", "Jorge"),
				new Amigo("64218524V", "Ruben") };
		// 5 amigos.
		Persona persona = new Persona("71154879D", "Mario", amigos,
				new Conocido[] { new Conocido("87935167A", "Manuel") });
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("58932789G", "Jorge") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("64218524V", "Ruben"),
						new Conocido("16422435K", "Pedro") });
		Persona amigo4 = new Persona("58932789G", "Jorge",
				new Amigo[] { new Amigo("71154879D", "Mario"),
						new Amigo("12345678Z", "Pepe") },
				new Conocido[] { new Conocido("56832445C", "Regino") });
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra a la cola de amigos y reserva para Pepe y para Juan.
		colaAmigos.entrarEnCola(amigo1, new Persona[] { amigo4 });
		// Entra Pepe e intenta reservar para su amigo Jorge, no puede por la
		// condicion de que una persona que ha sido reservada ya no puede
		// reservar.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaAmigosYaColados() {
		Amigo[] amigos = new Amigo[] { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"),
				new Amigo("16422435K", "Pedro"),
				new Amigo("58932789G", "Jorge"),
				new Amigo("64218524V", "Ruben") };
		// 5 amigos.
		Persona persona = new Persona("71154879D", "Mario", amigos,
				new Conocido[] { new Conocido("87935167A", "Manuel") });
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") }, new Conocido[] {
				new Conocido("64218524V", "Ruben"),
				new Conocido("16422435K", "Pedro") });
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra a la cola de amigos y reserva para Pepe y para Juan.
		Persona amigo3 = new Persona("56832445C", "Regino",
				new Amigo[] { new Amigo("12345678Z", "Pepe"),
						new Amigo("54673829T", "Juan") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		colaAmigos.entrarEnCola(amigo3, new Persona[] { amigo1, amigo2 });
		// Regino intenta entrar en la cola e intenta reservar para Pepe y Juan.
		// Se deberia mostrar una excepcion diciendo que esos amigos ya han sido
		// reservados
		// por otra persona que ha entrado antes en la cola!
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaConAmigoNoExistente() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo7 = new Persona("84723593B", "Senmao",
				new Amigo[] { new Amigo("12345678Z", "Pepe"),
						new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("16422435K", "Pedro") });
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo7 });
		// El amigo para el que se quiere reservar no es un amigo de la persona
		// que
		// va a entrar a la cola.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaNoAmigosDePersona() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe",
				new Amigo[] { new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("16422435K", "Pedro") });
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Pepe no tiene como amigo a Mario y por tanto no se puede reservar
		// como amigos
		// a gente que no son tus amigos realmente!
	}

	@Test
	public void getAmigosReservadosPersonaValida() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") }, new Conocido[] {
				new Conocido("64218524V", "Ruben"),
				new Conocido("16422435K", "Pedro") });
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra en la cola de amigos y reserva para Pepe y Juan.
		int amigosReservados = colaAmigos.getAmigosReservados(persona);
		// los amigos reservados de Mario son 2 (Pepe y Juan).
		assertNotEquals(amigosReservados, 0);
		assertEquals(amigosReservados, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosReservadosPersonaNull() {
		colaAmigos.getAmigosReservados(null);
		// persona no puede ser valor nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosReservadosPersonaNoEnCola() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.getAmigosReservados(persona);
		// La persona no esta en cola y por tanto no se puede saber el numero de
		// amigos reservados.
	}

	@Test
	public void reservarAmigosPersonaEnCola() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.entrarEnCola(persona);
		// Entra Mario a la cola pero sin realizar reservas en ese momento.
		// Mario puede realizar reservas en la cola siempre y cuando no se pase
		// de 10 amigos reservados.
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") }, new Conocido[] {
				new Conocido("64218524V", "Ruben"),
				new Conocido("16422435K", "Pedro") });
		colaAmigos.reservar(persona, new Persona[] { amigo1, amigo2 });
		assertNotNull(colaAmigos.getColaAmigos());
		Persona[] personasEsperadas = { persona };
		Persona[] personasObtenidas = colaAmigos.getColaAmigos();
		assertArrayEquals(personasEsperadas, personasObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarAmigosPersonaNull() {
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") }, new Conocido[] {
				new Conocido("64218524V", "Ruben"),
				new Conocido("16422435K", "Pedro") });
		colaAmigos.reservar(null, new Persona[] { amigo1, amigo2 });
		// persona no puede ser un valor nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarAmigosAmigosNull() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.reservar(persona, null);
		// amigos no pueden ser un valor nulo
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaNoEnCola() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") }, new Conocido[] {
				new Conocido("64218524V", "Ruben"),
				new Conocido("16422435K", "Pedro") });
		colaAmigos.reservar(persona, new Persona[] { amigo1, amigo2 });
		// La persona no estaba en la cola a la hora de reservar.
		// Este metodo solo puede usarse cuando se esta en la cola.
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaAmigosYaColados() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("56832445C", "Regino") }, new Conocido[] {
				new Conocido("64218524V", "Ruben"),
				new Conocido("16422435K", "Pedro") });
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		for (Persona amigo : amigosReservados) {
			colaAmigos.entrarEnCola(amigo);
		}
		// Entran las personas que habia reservado Mario al entrar.
		Persona amigo3 = new Persona("58932789G", "Jorge",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("56832445C", "Regino") });
		colaAmigos.reservar(persona, new Persona[] { amigo3 });
		// Mario intenta de nuevo reservar para otro amigo suyo,
		// pero no puede porque ya se han colado todas las personas que habia
		// reservado al entrar en la cola.
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaAmigosMasDe10() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"),
				new Amigo("16422435K", "Pedro"),
				new Amigo("58932789G", "Jorge"),
				new Amigo("64218524V", "Ruben"), new Amigo("15377228A", "Ana"),
				new Amigo("84723593B", "Senmao"),
				new Amigo("56832445C", "Regino"),
				new Amigo("12367843F", "Daniel"),
				new Amigo("67832459P", "Sara"),
				new Amigo("87935167A", "Manuel") };
		Persona persona = new Persona("71154879D", "Mario", amigos,
				new Conocido[] {});
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("71154879D", "Mario"),
				new Amigo("58932789G", "Jorge") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("64218524V", "Ruben"),
						new Conocido("16422435K", "Pedro") });
		Persona amigo3 = new Persona("16422435K", "Pedro",
				new Amigo[] { new Amigo("71154879D", "Mario"),
						new Amigo("67832459P", "Sara") },
				new Conocido[] { new Conocido("12367843F", "Daniel") });
		Persona amigo4 = new Persona("58932789G", "Jorge",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("56832445C", "Regino") });
		Persona amigo5 = new Persona("64218524V", "Ruben", new Amigo[] {
				new Amigo("84723593B", "Senmao"),
				new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("54673829T", "Juan") });
		Persona amigo6 = new Persona("15377228A", "Ana",
				new Amigo[] { new Amigo("67832459P", "Sara"),
						new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("84723593B", "Senmao") });
		Persona amigo7 = new Persona("84723593B", "Senmao",
				new Amigo[] { new Amigo("12345678Z", "Pepe"),
						new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("16422435K", "Pedro") });
		Persona amigo8 = new Persona("56832445C", "Regino",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("15377228A", "Ana") });
		Persona amigo9 = new Persona("12367843F", "Daniel",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] {});
		Persona amigo10 = new Persona("67832459P", "Sara", new Amigo[] {
				new Amigo("58932789G", "Jorge"),
				new Amigo("71154879D", "Mario") }, new Conocido[] {});
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2, amigo3,
				amigo4, amigo5, amigo6, amigo7, amigo8, amigo9, amigo10 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Mario entra en cola y reserva para 10 amigos.
		Persona amigo11 = new Persona("87935167A", "Manuel",
				new Amigo[] { new Amigo("71154879D", "Mario") },
				new Conocido[] {});
		colaAmigos.reservar(persona, new Persona[] { amigo11 });
		// Mario intenta reservar en cola para un nuevo amigo mas, pero no
		// puede porque el limite maximo de amigos que se pueden reservar es de
		// 10.
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaAmigosIguales() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.entrarEnCola(persona);
		Persona amigo1 = new Persona("12345678Z", "Pepe",
				new Amigo[] { new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		colaAmigos.reservar(persona, new Persona[] { amigo1, amigo1 });
		// No se puede reservar amigos iguales e identicos para una persona que
		// este en la cola
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaAmigosNoExistentes() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe",
				new Amigo[] { new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan",
				new Amigo[] { new Amigo("56832445C", "Regino") },
				new Conocido[] { new Conocido("64218524V", "Ruben"),
						new Conocido("16422435K", "Pedro") });
		colaAmigos.entrarEnCola(persona);
		colaAmigos.reservar(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra en la cola y reserva para dos amigos suyos: Pepe y Juan,
		// ambos son amigos suyos pero en realidad no, porque Pepe y Juan no
		// tienen
		// como amigo a Mario, por tanto es un caso no valido.
	}

	@Test
	public void getAmigosPosiblesDeReservarPersonaValida() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("56832445C", "Regino"),
				new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1 });
		// Mario entra en la cola y reserva solo para su amigo Pepe.
		int numAmigosPorReservar = colaAmigos
				.getAmigosPorReservar(persona);
		assertNotEquals(numAmigosPorReservar, 0);
		assertEquals(numAmigosPorReservar, 9);
		// Por tanto calculamos el numero de amigos de la persona que aun puede
		// reservar: 9, porque ya ha reservado para su amigo Pepe, pero aun
		// puede reservar para 9 personas mas.
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosPosiblesDeReservarPersonaNull() {
		int numAmigosPorReservar = colaAmigos.getAmigosPorReservar(null);
		// persona no puede ser un valor nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosPosiblesDeReservarPersonaNoEnCola() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		int numAmigosPorReservar = colaAmigos
				.getAmigosPorReservar(persona);
		// persona no esta en cola! NO se puede determinar el numero de amigos
		// de una persona que no esta en la cola de amigos.
	}

	@Test
	public void getAmigosDelantePersonaEnCola() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona amigo1 = new Persona("12345678Z", "Pepe", new Amigo[] {
				new Amigo("56832445C", "Regino"),
				new Amigo("71154879D", "Mario") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		Persona amigo2 = new Persona("54673829T", "Juan", new Amigo[] {
				new Amigo("56832445C", "Regino"),
				new Amigo("71154879D", "Mario") }, new Conocido[] {
				new Conocido("64218524V", "Ruben"),
				new Conocido("16422435K", "Pedro") });
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2 };
		Persona amigo3 = new Persona("15377228A", "Ana",
				new Amigo[] { new Amigo("67832459P", "Sara") },
				new Conocido[] { new Conocido("84723593B", "Senmao") });
		colaAmigos.entrarEnCola(amigo3);
		// Ana entra a la cola.
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Mario entra y reserva para Pepe y Juan
		for (Persona amigo : amigosReservados) {
			colaAmigos.entrarEnCola(amigo);
		}
		// Pepe y Juan entran en la cola
		Persona[] amigosDelante = colaAmigos.getAmigosDelante(persona);
		assertNotNull(amigosDelante);
		assertArrayEquals(amigosDelante, amigosReservados);
		// La cantidad de amigos delante de Mario son solo dos:
		// Pepe y Juan, aunque el estado de la cola sea: Ana, Pepe, Juan, Mario.
		Persona[] estadoCola = colaAmigos.getColaAmigos();
		Persona[] estadoEsperado = { amigo3, amigo1, amigo2, persona };
		assertArrayEquals(estadoEsperado, estadoCola);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosDelantePersonaNull() {
		Persona[] amigosDelante = colaAmigos.getAmigosDelante(null);
		// persona no puede ser valor nulo
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosDelantePersonaNoEnCola() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("58932789G", "Jorge"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		Persona[] amigosDelante = colaAmigos.getAmigosDelante(persona);
		// la persona necesita estar en la cola de amigos para poder saber
		// los amigos de delante colados actualmente.
	}

	@Test
	public void getPrimeraPersonaCola() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.entrarEnCola(persona);
		Persona personaPrimera = colaAmigos.getPrimeraPersona();
		assertNotNull(personaPrimera);
		assertEquals(personaPrimera, persona);
	}

	@Test(expected = IllegalStateException.class)
	public void getPrimeraPersonaColaSinPersonas() {
		Persona personaPrimera = colaAmigos.getPrimeraPersona();
	}

	@Test
	public void atenderPersonaCola() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.entrarEnCola(persona);
		assertNotNull(colaAmigos.getColaAmigos());
		assertEquals(colaAmigos.getColaAmigos().length, 1);
		colaAmigos.atender();
		assertEquals(colaAmigos.getColaAmigos().length, 0);
	}

	@Test(expected = IllegalStateException.class)
	public void atenderSinPersonas() {
		colaAmigos.atender();
	}

	@Test
	public void getPersonasColaAmigos() {
		Amigo[] amigos = { new Amigo("12345678Z", "Pepe"),
				new Amigo("54673829T", "Juan"), new Amigo("58932789G", "Jorge") };
		Conocido[] conocidos = { new Conocido("16422435K", "Pedro"),
				new Conocido("64218524V", "Ruben") };
		Persona persona = new Persona("71154879D", "Mario", amigos, conocidos);
		colaAmigos.entrarEnCola(persona);
		Persona[] personasObtenidas = colaAmigos.getColaAmigos();
		assertNotNull(personasObtenidas);
		Persona[] personasEsperadas = new Persona[] { persona };
		assertArrayEquals(personasEsperadas, personasObtenidas);
	}
}
