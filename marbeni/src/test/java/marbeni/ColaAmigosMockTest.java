package marbeni;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;
import org.easymock.*;

public class ColaAmigosMockTest {

	private ColaAmigos colaAmigos;
	
	@Mock
	private Persona persona;
	private Persona amigo1;
	private Persona amigo2;
	private Persona amigo3;
	private Persona amigo4;
	private Persona amigo5;
	private Persona amigo6;
	private Persona amigo7;
	private Persona amigo8;
	private Persona amigo9;
	private Persona amigo10;
	private Persona amigo11;
	
	@Before
	public void setUp() throws Exception {
		colaAmigos = new ColaAmigos ();
		persona = createMock(Persona.class);
		amigo1 = createMock(Persona.class);
		amigo2 = createMock(Persona.class);
		amigo3 = createMock(Persona.class);
		amigo4 = createMock(Persona.class);
		amigo5 = createMock(Persona.class);
		amigo6 = createMock(Persona.class);
		amigo7 = createMock(Persona.class);
		amigo8 = createMock(Persona.class);
		amigo9 = createMock(Persona.class);
		amigo10 = createMock(Persona.class);
		amigo11 = createMock(Persona.class);
		expect(persona.isAmigo(amigo1)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo2)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo3)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo4)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo5)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo6)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo7)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo8)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo9)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo10)).andReturn(true).anyTimes();
		expect(persona.isAmigo(amigo11)).andReturn(true).anyTimes();
		expect(amigo1.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo2.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo3.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo4.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo5.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo6.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo7.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo8.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo9.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo10.isAmigo(persona)).andReturn(true).anyTimes();
		expect(amigo11.isAmigo(persona)).andReturn(true).anyTimes();
		replay(persona);
		replay(amigo1);
		replay(amigo2);
		replay(amigo3);
		replay(amigo4);
		replay(amigo5);
		replay(amigo6);
		replay(amigo7);
		replay(amigo8);
		replay(amigo9);
		replay(amigo10);
		replay(amigo11);
	}

	@After
	public void tearDown() throws Exception {
		colaAmigos = null;
		persona = null;
		amigo1 = null;
		amigo2 = null;
		amigo3 = null;
		amigo4 = null;
		amigo5 = null;
		amigo6 = null;
		amigo7 = null;
		amigo8 = null;
		amigo9 = null;
		amigo10 = null;
		amigo11 = null;
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaYaDentroSinReserva() {
		colaAmigos.entrarEnCola(persona);
		// Entra por primera vez.
		colaAmigos.entrarEnCola(persona);
		// persona no puede volver a entrar a la cola si ya esta dentro.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaNullSinReserva() {
		colaAmigos.entrarEnCola(null);
		// persona no puede ser null.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaNullConReserva() {
		Persona[] amigos = { new Persona("12345678Z", "Pepe"),
				new Persona("54673829T", "Juan") };
		colaAmigos.entrarEnCola(null, amigos);
		// persona no puede ser null.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaAmigosNullConReserva() {
		colaAmigos.entrarEnCola(persona, null);
		// amigos no pueden ser valores nulos.
	}

	@Test
	public void entrarEnColaConMenosDeDiezAmigos() {
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Cumple con las condiciones.
		// Los amigos para los que se reservan no son mas de 10.
		for (Persona p : amigosReservados) {
			colaAmigos.entrarEnCola(p);
			// Los amigos entran y como estan reservados, se adelantan en la
			// cola.
		}
		Persona[] colaAmigosEsperada = { amigo1, amigo2, persona };
		// Este es el orden de la cola tras la entrada de la persona y amigos.
		Persona[] colaAmigosObtenida = colaAmigos.getColaAmigos();
		assertNotNull(colaAmigos.getColaAmigos());
		assertArrayEquals(colaAmigosEsperada, colaAmigosObtenida);
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonasIguales() {
		Persona[] amigosReservados = new Persona[] { amigo1, amigo1 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Se intenta reservar para dos personas iguales!
	}

	@Test
	public void entrarEnColaConDiezAmigos() {
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2, amigo3,
				amigo4, amigo5, amigo6, amigo7, amigo8, amigo9, amigo10 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Todos son amigos de Mario.
		for (Persona p : amigosReservados) {
			colaAmigos.entrarEnCola(p);
		}
		Persona[] colaAmigosEsperada = { amigo1, amigo2, amigo3, amigo4,
				amigo5, amigo6, amigo7, amigo8, amigo9, amigo10, persona };
		Persona[] colaAmigosObtenida = colaAmigos.getColaAmigos();
		assertNotNull(colaAmigos.getColaAmigos());
		assertArrayEquals(colaAmigosEsperada, colaAmigosObtenida);
		// Cumple con las condiciones.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaConMasDe10Amigos() {
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2, amigo3,
				amigo4, amigo5, amigo6, amigo7, amigo8, amigo9, amigo10,
				amigo11 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// No se permite reservar para mas de 10 amigos.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaYaDentro() {
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra a la cola de amigos y reserva para dos amigos.
		// Aqui no hay ninguna excepcion ni ningun problema.
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo3, amigo4 });
		// Se vuelve a intentar entrar en la cola por parte de la persona Mario.
		// Pero no puede volver a entrar y realizar reserva en la cola porque ya
		// esta dentro.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaPersonaNoPuedeReservar() {
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra a la cola de amigos y reserva para Pepe y para Juan.
		colaAmigos.entrarEnCola(amigo1, new Persona[] { amigo4 });
		// Entra Pepe e intenta reservar para su amigo Jorge, no puede por la
		// condicion de que una persona que ha sido reservada ya no puede
		// reservar.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaAmigosYaColados() {
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra a la cola de amigos y reserva para Pepe y para Juan.
		Persona amigo3 = new Persona("56832445C", "Regino",
				new Amigo[] { new Amigo("12345678Z", "Pepe"),
						new Amigo("54673829T", "Juan") },
				new Conocido[] { new Conocido("64218524V", "Ruben") });
		colaAmigos.entrarEnCola(amigo3, new Persona[] { amigo1, amigo2 });
		// Regino intenta entrar en la cola e intenta reservar para Pepe y Juan.
		// Se deberia mostrar una excepcion diciendo que esos amigos ya han sido
		// reservados
		// por otra persona que ha entrado antes en la cola!
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaConAmigoNoExistente() {
		Persona persona = createMock(Persona.class);
		Persona amigo = createMock(Persona.class);
		expect(persona.isAmigo(amigo)).andReturn(false).anyTimes();
		replay(persona);
		replay(amigo);
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo });
		// El amigo para el que se quiere reservar no es un amigo de la persona
		// que
		// va a entrar a la cola.
	}

	@Test(expected = IllegalArgumentException.class)
	public void entrarEnColaNoAmigosDePersona() {
		Persona persona = createMock(Persona.class);
		Persona amigo1 = createMock(Persona.class);
		Persona amigo2 = createMock(Persona.class);
		expect(persona.isAmigo(amigo1)).andReturn(false).anyTimes();
		expect(persona.isAmigo(amigo2)).andReturn(true).anyTimes();
		expect(amigo1.isAmigo(persona)).andReturn(false).anyTimes();
		expect(amigo2.isAmigo(persona)).andReturn(true).anyTimes();
		replay(persona);
		replay(amigo1);
		replay(amigo2);
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Pepe no tiene como amigo a Mario y por tanto no se puede reservar
		// como amigos
		// a gente que no son tus amigos realmente!
	}

	@Test
	public void getAmigosReservadosPersonaValida() {
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra en la cola de amigos y reserva para Pepe y Juan.
		int amigosReservados = colaAmigos.getAmigosReservados(persona);
		// los amigos reservados de Mario son 2 (Pepe y Juan).
		assertNotEquals(amigosReservados, 0);
		assertEquals(amigosReservados, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosReservadosPersonaNull() {
		colaAmigos.getAmigosReservados(null);
		// persona no puede ser valor nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosReservadosPersonaNoEnCola() {
		colaAmigos.getAmigosReservados(persona);
		// La persona no esta en cola y por tanto no se puede saber el numero de
		// amigos reservados.
	}

	@Test
	public void reservarAmigosPersonaEnCola() {
		colaAmigos.entrarEnCola(persona);
		// Entra Mario a la cola pero sin realizar reservas en ese momento.
		// Mario puede realizar reservas en la cola siempre y cuando no se pase
		// de 10 amigos reservados.
		colaAmigos.reservar(persona, new Persona[] { amigo1, amigo2 });
		assertNotNull(colaAmigos.getColaAmigos());
		Persona[] personasEsperadas = { persona };
		Persona[] personasObtenidas = colaAmigos.getColaAmigos();
		assertArrayEquals(personasEsperadas, personasObtenidas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarAmigosPersonaNull() {
		colaAmigos.reservar(null, new Persona[] { amigo1, amigo2 });
		// persona no puede ser un valor nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarAmigosAmigosNull() {
		colaAmigos.reservar(persona, null);
		// amigos no pueden ser un valor nulo
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaNoEnCola() {
		colaAmigos.reservar(persona, new Persona[] { amigo1, amigo2 });
		// La persona no estaba en la cola a la hora de reservar.
		// Este metodo solo puede usarse cuando se esta en la cola.
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaAmigosYaColados() {
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		for (Persona amigo : amigosReservados) {
			colaAmigos.entrarEnCola(amigo);
		}
		// Entran las personas que habia reservado Mario al entrar.
		colaAmigos.reservar(persona, new Persona[] { amigo3 });
		// Mario intenta de nuevo reservar para otro amigo suyo,
		// pero no puede porque ya se han colado todas las personas que habia
		// reservado al entrar en la cola.
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaAmigosMasDe10() {
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2, amigo3,
				amigo4, amigo5, amigo6, amigo7, amigo8, amigo9, amigo10 };
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Mario entra en cola y reserva para 10 amigos.
		colaAmigos.reservar(persona, new Persona[] { amigo11 });
		// Mario intenta reservar en cola para un nuevo amigo mas, pero no
		// puede porque el limite maximo de amigos que se pueden reservar es de
		// 10.
	}

	@Test(expected = IllegalArgumentException.class)
	public void reservarPersonaAmigosNoExistentes() {
		Persona persona = createMock(Persona.class);
		Persona amigo1 = createMock(Persona.class);
		Persona amigo2 = createMock(Persona.class);
		expect(persona.isAmigo(amigo1)).andReturn(false).anyTimes();
		expect(persona.isAmigo(amigo2)).andReturn(false).anyTimes();
		expect(amigo1.isAmigo(persona)).andReturn(false).anyTimes();
		expect(amigo2.isAmigo(persona)).andReturn(false).anyTimes();
		replay(persona);
		replay(amigo1);
		replay(amigo2);
		colaAmigos.entrarEnCola(persona);
		colaAmigos.reservar(persona, new Persona[] { amigo1, amigo2 });
		// Mario entra en la cola y reserva para dos amigos suyos: Pepe y Juan,
		// ambos son amigos suyos pero en realidad no, porque Pepe y Juan no
		// tienen
		// como amigo a Mario, por tanto es un caso no valido.
	}

	@Test
	public void getAmigosPosiblesDeReservarPersonaValida() {
		colaAmigos.entrarEnCola(persona, new Persona[] { amigo1 });
		// Mario entra en la cola y reserva solo para su amigo Pepe.
		int numAmigosPorReservar = colaAmigos
				.getAmigosPorReservar(persona);
		assertNotEquals(numAmigosPorReservar, 0);
		assertEquals(numAmigosPorReservar, 9);
		// Por tanto calculamos el numero de amigos de la persona que aun puede
		// reservar: 9, porque ya ha reservado para su amigo Pepe, pero aun
		// puede reservar para 9 personas mas.
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosPosiblesDeReservarPersonaNull() {
		int numAmigosPorReservar = colaAmigos.getAmigosPorReservar(null);
		// persona no puede ser un valor nulo.
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosPosiblesDeReservarPersonaNoEnCola() {
		int numAmigosPorReservar = colaAmigos
				.getAmigosPorReservar(persona);
		// persona no esta en cola! NO se puede determinar el numero de amigos
		// de una persona que no esta en la cola de amigos.
	}

	@Test
	public void getAmigosDelantePersonaEnCola() {
		Persona[] amigosReservados = new Persona[] { amigo1, amigo2 };
		colaAmigos.entrarEnCola(amigo3);
		// Ana entra a la cola.
		colaAmigos.entrarEnCola(persona, amigosReservados);
		// Mario entra y reserva para Pepe y Juan
		for (Persona amigo : amigosReservados) {
			colaAmigos.entrarEnCola(amigo);
		}
		// Pepe y Juan entran en la cola
		Persona[] amigosDelante = colaAmigos.getAmigosDelante(persona);
		assertNotNull(amigosDelante);
		assertArrayEquals(amigosDelante, amigosReservados);
		// La cantidad de amigos delante de Mario son solo dos:
		// Pepe y Juan, aunque el estado de la cola sea: Ana, Pepe, Juan, Mario.
		Persona[] estadoCola = colaAmigos.getColaAmigos();
		Persona[] estadoEsperado = {amigo3, amigo1, amigo2, persona};
		assertArrayEquals(estadoEsperado, estadoCola);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosDelantePersonaNull() {
		Persona[] amigosDelante = colaAmigos.getAmigosDelante(null);
		// persona no puede ser valor nulo
	}

	@Test(expected = IllegalArgumentException.class)
	public void getAmigosDelantePersonaNoEnCola() {
		Persona[] amigosDelante = colaAmigos.getAmigosDelante(persona);
		// la persona necesita estar en la cola de amigos para poder saber
		// los amigos de delante colados actualmente.
	}

	@Test
	public void getPrimeraPersonaCola() {
		colaAmigos.entrarEnCola(persona);
		Persona personaPrimera = colaAmigos.getPrimeraPersona();
		assertNotNull(personaPrimera);
		assertEquals(personaPrimera, persona);
	}

	@Test(expected = IllegalStateException.class)
	public void getPrimeraPersonaColaSinPersonas() {
		Persona personaPrimera = colaAmigos.getPrimeraPersona();
	}

	@Test
	public void atenderPersonaCola() {
		colaAmigos.entrarEnCola(persona);
		assertNotNull(colaAmigos.getColaAmigos());
		assertEquals(colaAmigos.getColaAmigos().length, 1);
		colaAmigos.atender();
		assertEquals(colaAmigos.getColaAmigos().length, 0);
	}

	@Test(expected = IllegalStateException.class)
	public void atenderSinPersonas() {
		colaAmigos.atender();
	}

	@Test
	public void getPersonasColaAmigos() {
		colaAmigos.entrarEnCola(persona);
		Persona[] personasObtenidas = colaAmigos.getColaAmigos();
		assertNotNull(personasObtenidas);
		Persona[] personasEsperadas = new Persona[] { persona };
		assertArrayEquals(personasEsperadas, personasObtenidas);
	}
}
