package marbeni;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConocidoTest {

	@Test
	public void crearConocidoValido () {
		Relacion conocido = new Conocido("71154879D", "Mario");
		assertEquals(conocido.getDNI(), "71154879D");
		assertEquals(conocido.getNombre(), "Mario");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearConocidoDNINull() {
		Relacion conocido = new Conocido(null, "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearConocidoDNIVacio() {
		Relacion conocido = new Conocido("", "Mario");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearConocidoDNINoNueveCifras() {
		Relacion conocido = new Conocido("12343453454362", "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearConocidoDNINoOchoDigitos() {
		Relacion conocido = new Conocido("1234", "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearConocidoDNINoUltimaLetra() {
		Relacion conocido = new Conocido("12345678?", "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearConocidoDNILetraNoValida() {
		Relacion conocido = new Conocido("12345678K", "Mario");
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearConocidoNombreNull() {
		Relacion conocido = new Conocido("71154879D", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void crearConocidoNombreVacio() {
		Relacion conocido = new Conocido("71154879D", "");
	}

}
