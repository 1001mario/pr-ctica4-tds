package marbeni;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AmigoTest.class, ColaAmigosTest.class, ConocidoTest.class,
		PersonaTest.class })
public class EjecutarTestsTDD {

}
