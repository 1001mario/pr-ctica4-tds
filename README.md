# README #

Este readme informa acerca de como ejecutar la aplicación o el proyecto determinado.

### ¿Para qué es este repositorio? ###

Este repositorio centralizado almacena un proyecto eclipse para jdk 7, el cual contiene una serie de clases que representan a amigos, conocidos (relaciones de personas), personas y quizá la clase más importante de todas, colas de amigos.

Las colas de amigos están formadas por personas que hacen cola, se van atendiendo a cada una de esas personas que están en la cola pero tiene una diferencia esencial con respecto a otras colas y es que cuando una persona entra en la cola puede reservar para un conjunto de amigos determinados, siempre y cuando el número de amigos invitado es de 10 o menos.

Exiten mas condiciones en la cola de amigos, las cuales vienen explicadas en el javadoc específico de cada una de las clases y en los tests junits también se muestran ejemplos de como se deben usar las clases y las condiciones determinadas que se tienen que cumplir debido al desiderata concreto de la práctica, el cual expondré a continuación.

### Desiderata de la práctica ###

Se desea tener un tipo especial de Cola que llamaremos “Cola de amigos”.
Una cola de amigos describe una cola en la que se debe pedir la vez para
entrar y en la que se puede reservar sitio para un grupo de amigos. Así, al
llegar una nueva persona a la cola, si algún amigo suyo le ha reservado sitio,
puede colocarse justo delante de su amigo, en lugar de pedir la vez, ya que
su amigo lo hizo para él en su momento.

Para evitar situaciones complicadas de aceptar por el resto de personas en la
cola, como por ejemplo que todo el grupo de Ingeniería Informática Mención
Ingeniería del Software pueda colarse porque uno pidió la vez reservando
para 40, se establecen las siguientes limitaciones:

* No se aceptará que se entre en la cola reservando para más de 10
amigos.
* Si una persona ha reservado para n amigos, después de colar a los n
indicados ya no podrá colar a nadie más.
* Un amigo que ha sido colado por otro no puede a su vez colar a nadie
más.
* Una persona que esté en la cola no puede volver a entrar en ella.

Como mínimo se desea tener funcionalidad en la cola de amigos que
permita:

* Pedir la vez: cuando una persona llega a la cola, aporta el número de
amigos para los que desea reservar. Puede no reservar para nadie
más. Al que pide la vez se le coloca el último en la cola.
* Colarse con el que está en la cola: cuando una nueva persona llega a
la cola, comprueba si tiene un amigo que ya está en ella y que haya
reservado. Si se trata de uno de sus amigos, le permite colocar al
nuevo en la cola justo por delante del amigo que en su momento
reservó en la cola pidiendo la vez (siempre atendiendo a las
restricciones antes indicadas), y garantizando que quien está en la
cola desea colarle como uno de sus amigos.
* Se desea saber con cuántos amigos dijo una persona que venía.
* Se desea saber, dada una persona que está en la cola, a cuántos
amigos puede colar aún.
* Se desea saber para una persona que está en la cola, cuáles de los
que están delante son los amigos para los que pidió la vez
* Se desea saber a qué persona le tocaría ser atendido según el orden
de la cola.
* Se desea atender al que le toca. En este caso sería simplemente que
ya deje de estar en la cola.

En el caso de una persona, se desea poder saber si otra persona es su
amigo, así como saber todos sus amigos, se desea poder hacer que la
persona conozca a otras personas, y que se haga amigo de ellos. Se
diferencia entre conocidos y amigos. Primero alguien debe ser conocido para
después ser amigo. También se permitirá dejar de tener amistad con uno de
los actuales amigos, que pasaría a ser otra vez simplemente un conocido.

### ¿Cómo ejecuto o despliego la aplicación? ###

Existe un script en el proyecto eclipse del repositorio que se denomina build.xml, este tiene una serie de tareas especificadas y concretas que nombraré a continuación: compilar, limpiar, init (inicializar), ejecutar, ejecutarTestsTDD, ejecutarTestEnAislamiento, obtenerInformeCobertura y documentar. Este es el conjunto de objetivos o tareas que tiene el script ant y que puede ser utilizado para construir y ejecutar la aplicación en sí. De hecho, el proyecto Java no tiene los archivos .class que se crean en el proceso de compilación de las clases y tests de la aplicación, por tanto es necesario que utilices este script ant!

La funcionalidad viene determinada por ant y es muy sencillo: desde consola o terminal, se puede determinar al script que funcionalidad o tarea hacer de la siguiente manera y con la siguiente sintaxis:

ant => Si se ejecuta este comando por terminal se consigue realizar la compilación, esto se debe a que por defecto el script compila si no se le pasan parámetros como argumento al comando.

ant limpiar => Elimina la carpeta target del proyecto y las clases compiladas de formato .class, elimina la carpeta doc que contenía la información asociada al javadoc de las clases y la carpeta reporteJunit que contenia la información de los tests de cobertura de cada una de las clases implementadas.

ant init => Se crea la carpeta target en la cual almacena dentro dos carpetas, una para las clases implementadas: classes y otra para las clases asociadas a los tests de cobertura: test-classes. En estas carpetas se van a almacenar los .class del proceso de compilación de archivos.

ant compilar => Compila todas las clases del proyecto, elaborando el siguiente esquema: las clases implementadas dentro de src/java/main/marbeni se compilan en target/classes/ y las clases de tests situadas en src/java/test/marbeni se compilan y almacenan en formato .class cada una de ellas en target/test-classes.

ant ejecutar => Ejecuta todos los tests, ya sean los de caja blanca o los de caja negra o secuencia, se muestra por consola el resultado de todos los tests (verde todos ellos tras realizar el ciclo rojo-verde-refactor) y se guardan los resultados de los mismos en la carpeta junitReporte.

ant ejecutarTestsTDD => Ejecuta los tests de caja negra, los que han determinado la especificación de la aplicación java y guarda los resultados en junitReporte

ant ejecutarTestEnAislamiento => Ejecuta los tests de caja blanca asociados con los mock objects y guarda los resultados en junitReporte.

ant obtenerInformeCobertura => muestra un informe de cobertura en la carpeta target/site/cobertura dentro del proyecto marbeni correspondiente a las clases implementadas en el paquete src/java/main/marbeni. Además se establecen los resultados de la ejecución de los tests en la carpeta surefire-reports de target/ situada en el interior del proyecto java.

ant documentar => Javadoc de las clases en la carpeta doc del proyecto (tanto para las clases implementadas como para los tests).

También se puede ejecutar los tests con eclipse luna, siempre y cuando se tenga jdk en la versión 1.7, puesto que es una de las condiciones de la práctica para el proyecto.

### ¿Autores del proyecto? ###

* Dueño y autor del respositorio: Mario Benito Rodríguez, alias: 1001mario.